try:
    from vagrant import vagrant
except ImportError:
    pass

import cuisine
from fabric.api import *
import os
import fabtools

INSTALL_PATH = "/opt/"
MODOBOA_SRC_PATH = os.path.join(INSTALL_PATH, 'modoboa')
PASS = 'zxc'
DOMAIN = 'localhost'
UWSGI_INI_PATH = INSTALL_PATH + "uwsgi.ini"


def __create_databases(db_name, user, password):
    query =[
         "drop database if  exists  %s;" % db_name,
         "create database  %s;" % db_name,
          "grant all privileges on %s.* to %s@'localhost' identified by '%s';" %(db_name, user, password),
          ]
    __executequery(query)

def __executequery(query):
    run(' mysql --user=root --execute="%s" ' %  "".join(query))

@task
def setup_amavis():

    __create_databases("amavis", 'amavis_user', PASS)

    kwargs = {
        'driver':'mysql',
        'database':'amavis',
        'dbhost':'localhost',
        'dbport':'3306',
        'dbuser':'amavis_user',
        'password':PASS,
        }

    conf_amavis = cuisine.text_strip_margin("""
      |use strict;
      |@lookup_sql_dsn = (['DBI:%(driver)s:database=%(database)s;host=%(dbhost)s;port=%(dbport)s', '%(dbuser)s', '%(password)s]']);
      |@storage_sql_dsn =
      |  (['DBI:%(driver)s:database=%(database)s;host=%(dbhost)s;port=%(dbport)s', '%(dbuser)s', '%(password)s]']);
      |  $virus_quarantine_method = 'sql:';
      |  $spam_quarantine_method = 'sql:';
      |  $banned_files_quarantine_method = 'sql:';
      |  $bad_header_quarantine_method = 'sql:';
      |  $virus_quarantine_to = 'virus-quarantine';
      |  $banned_quarantine_to = 'banned-quarantine';
      |  $bad_header_quarantine_to = 'bad-header-quarantine';
      |  $spam_quarantine_to = 'spam-quarantine';
      |1;
    """ % kwargs)
    cuisine.file_write("/etc/amavis/conf.d/50-user", conf_amavis, sudo = True)
    fabtools.service.restart("amavis") 
    put("amavis_new.sql", "/tmp/")
    run("mysql -uroot amavis <  /tmp/amavis_new.sql")


@task
def uwsgi_settings(chdir=INSTALL_PATH+"/modoboa_site/" ,
        wsgi_file = INSTALL_PATH+"/modoboa_site/modoboa_site/wsgi.py",
        ini_file = UWSGI_INI_PATH
        ):
   conf = cuisine.text_strip_margin("""
        |[uwsgi]
        |socket=localhost:8000
        |master
        |chdir=%s
        |wsgi-file=%s
        """ % ( chdir,wsgi_file ))
   cuisine.file_write(ini_file, conf)
   fabtools.supervisor.restart_process("uwsgi")

@task 
def supervisor_settings():
    conf = cuisine.text_strip_margin("""
            |[program:uwsgi]
            |command=uwsgi %s 
            |stopsignal=QUIT
            |autorestart=true
            |autostart=true
            |redirect_stderr=true
       """ % UWSGI_INI_PATH)
    cuisine.file_write("/etc/supervisor/conf.d/uwsgi.conf", conf, sudo=True)
    fabtools.supervisor.update_config()


@task
def nginx_settings(install_path = INSTALL_PATH + "modoboa_site"):
    conf = cuisine.text_strip_margin("""
    | server {
    |   set $sname %s;
    |   server_name *.$sname $sname;
    |   listen 80;
    |   set $rootfolder %s;
    |   location /sitestatic {
    |           alias $rootfolder/sitestatic;
    |    }
    |   location /media {
    |       alias $rootfolder/media;
    |   }
    |  location / {
    |      include uwsgi_params;
    |          uwsgi_pass localhost:8000;
    |    }
    | }


    """ % (DOMAIN, install_path))

    cuisine.file_write("/etc/nginx/sites-enabled/default", conf, sudo=True)
    fabtools.service.reload("nginx")

@task
def prepare_modoboa():
    with cuisine.mode_sudo():
        cuisine.dir_ensure(INSTALL_PATH, recursive=True, mode='755', owner=env.user)
    if not cuisine.dir_exists(MODOBOA_SRC_PATH):
        run("mkdir -p %s" % INSTALL_PATH)
        run("git clone https://github.com/tonioo/modoboa.git %s" % MODOBOA_SRC_PATH)
    with cd(MODOBOA_SRC_PATH):
        sudo("python setup.py develop")
    with cuisine.mode_sudo():
        __create_databases("modoboa_site", 'modoboa_user', PASS)
    with cd(INSTALL_PATH):
        cuisine.dir_remove('modoboa_site')
        sudo(
                "modoboa-admin.py deploy modoboa_site"
                #" --syncdb "
                " --collectstatic "
                " --dburl  'mysql://%(user)s:%(upass)s@localhost/modoboa_site'"
                " --with-amavis"
                " --amavis_dburl  'mysql://%(auser)s:%(apass)s@localhost/amavis'"
                " --domain %(domain)s" % dict(
                    user='modoboa_user', 
                    upass=PASS,
                    auser='amavis_user',
                    apass=PASS,
                    domain = DOMAIN)
            )
         
@task
def install_requires():
    #cuisine.package_update()
    packages = [
        "git",
       #"libcairo2-dev",
       # "libpango1.0-dev" ,
       # "librrd-dev",
        "python-dev",
        "python-rrdtool",
        "python-pip",
        "python-lxml",
        "python-crypto",
        "python-argh",
        "python-chardet",
        "python-factory-boy",
        "python-passlib",
    #webinterface
        "mysql-server",
        "python-mysqldb",
        "mysql-client",
        "nginx",
        "supervisor",
    #server deps
        "dovecot-imapd",
        "postfix",
        "amavisd-new",
        "postfix-policyd-spf-python",
    ]
    for p in packages:
        cuisine.package_ensure(p)
    sudo("pip install uwsgi")

@task
def deploy():
    
    install_requires()
    prepare_modoboa()
    nginx_settings()
    uwsgi_settings()
    supervisor_settings()
    setup_amavis()
