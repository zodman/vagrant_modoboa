import cuisine
from fabric.api import  *
import pipes

@task
def vagrant():
    """
    Vagrant machine environ.
    """
    def ssh_config(name=''):
        """
        Get the SSH parameters for connecting to a vagrant VM.
        """
        with settings(hide('running')):
            output = local('vagrant ssh-config %s' % name, capture=True)

        config = {}
        for line in output.splitlines()[1:]:
            key, value = line.strip().split(' ', 2)
            config[key] = value
        return config

    def _settings_dict(config):
        settings = {}

        user = config['User']
        hostname = config['HostName']
        port = config['Port']

        # Build host string
        host_string = "%s@%s:%s" % (user, hostname, port)

        settings['user'] = user
        settings['hosts'] = [host_string]
        settings['host_string'] = host_string

        # Strip leading and trailing double quotes introduced by vagrant 1.1
        settings['key_filename'] = config['IdentityFile'].strip('"')

        settings['forward_agent'] = (config.get('ForwardAgent', 'no') == 'yes')
        settings['disable_known_hosts'] = True

        return settings

    config = ssh_config()
    extra_args = _settings_dict(config)
    env.update(extra_args)



